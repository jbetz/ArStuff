#!/usr/bin/python
# -*- coding: utf-8 -*- 




# *************************** class definitions *************************** 


class ClassA(object):

	def __init__(self):
		super(ClassA, self).__init__()
		print("ClassA Constructor")

	def printIt(self):
		print("ClassA print")

	def printAll(self):
		print("ClassA printAll")

	def tryPrintAll(self):
		print("ClassA tryPrintAll")


class ClassB(ClassA):

	def __init__(self):
		super(ClassB, self).__init__()
		print("ClassB Constructor")

	def printIt(self):
		print("ClassB print")

	def printAll(self):
		super(ClassB, self).printAll()
		print("ClassB printAll")

	def tryPrintAll(self):
		super(ClassB, self).tryPrintAll()
		print("ClassB tryPrintAll")


class ClassC(ClassA):

	def __init__(self):
		super(ClassC, self).__init__()
		print("ClassC Constructor")

	def printIt(self):
		print("ClassC print")

	def printAll(self):
		super(ClassC, self).printAll()
		print("ClassC printAll")

	def tryPrintAll(self):
		# Notice the missing call to super here! Without the trzPrintAll for ClassA is not called!
		# super(ClassC, self).tryPrintAll()            
		print("ClassC tryPrintAll")

	
class ClassD(ClassB, ClassC):

	def __init__(self):
		super(ClassD, self).__init__()  
		print("ClassD Constructor")

	def printIt(self):
		print("ClassD print")

	def printAll(self):
		super(ClassD, self).printAll()
		print("ClassD printAll")

	def tryPrintAll(self):
		super(ClassD, self).tryPrintAll()    
		print("ClassD tryPrintAll")


''' *************************** main script entry point *************************** '''
def main():
	print("Creating instance of ClassD, showing call to all constructors on the line according to __mro__ (using correct super call everywhere\n")
	aD = ClassD()

	print("\nCall to method of object of type ClassD without any super call, just plain 'overwride'\n")
	aD.printIt()

	print("\nCall to method of object of type ClassD with call to all base class methods with the same name\n")
	aD.printAll()

	print("\nCall to method of object of type ClassD with try to call to all base class methods with the same name but missing one single super call in the hirachy\n")
	aD.tryPrintAll()

	print("\nMethod resolution order for the final ClassD:")
	for (element, index) in zip(ClassD.__mro__, range(len(ClassD.__mro__))):
		print("%d. -> %s" % (index+1, str(element)))

	return


''' *************************** stand alone code *************************** '''
if __name__ == "__main__":
	main()


#EOF
