#!/usr/bin/env python


class A(object):
	def __init__(self):
		print("__init__ of class A")
		super(A, self).__init__()

	def printIt(self):
		print("This is print of class A")

	def specialFunctionA(self, data):
		print("Function only available from class A")

	def printAll(self):
		print("Print All from class A")
	

class B(object):
	def __init__(self):
		print("__init__ of class B")
		super(B, self).__init__()
		self.b = "I belong to b"
		    
	def printIt(self):
		print("This is print of class B")

	def specialFunctionB(self, data):
		print("Function only available from class B")

	def printAll(self):
		print("Print All from class B")


class C(A, B):
	def __init__(self):
		print("__init__ of class C")    
		super(C, self).__init__()
		self.c = "I belong to c"

	def printIt(self):
		print("This is print of class C")
		print("As you see you can access function of a specific class explicitly without using super. But if you add an additional class in the inheritance hirachy you would have to change your code here!")
		A.printIt(self)
		B.printIt(self)        

	def printAll(self):
		super(C, self).printAll()
		print("Print All from class C")


	def specialFunctionC(self, data):
		print("Function only available from class C")

    

class One(object):
	def __init__(self):
		print("__init__ of class One")
		self.one = "I belong to One"

class Two(object):
	def __init__(self):
		print("__init__ of class Two")
		self.two = "I belong to Two"
	                
class Three(One, Two):
	def __init__(self):
		print("__init__ of class Three")    
		super(Three, self).__init__()
		self.three = "I belong to Three"        

def main():

	print("\nCreating object of class A:")
	a = A()
	print("\nCreating object of class B:")
	b = B()
	print("\nCreating object of class C:")    
	c = C()


	print("\nLet's try to call the 'printIt' method of all previously created instances of A/B/C:")	
	print("\nCall printIt for a")
	a.printIt()
	
	print("\nCall printIt for b")	
	b.printIt()
	
	print("\nCall printIt for c")	
	c.printIt()        


	print("\nLet's try to do something similar but now with call to 'printAll' method of all previously created instances of A/B/C:")
	print("\nCall printAll for a")
	a.printAll()
	
	print("\nCall printAll for b")
	b.printAll()
	
	print("\nCall printAll for c")
	c.printAll()
	print("Uppps.. where is printAll for 'a'??")


	print("\nLet's see that we can call the functions of all classes we inherit from:")
	c.specialFunctionA(None)
	c.specialFunctionB(None)
	c.specialFunctionC(None)

	print("\nName resolution order for class C:")
	print(C.__mro__)


	print("\nAttention: See what happen if you miss to call super(X, self).__init__ in the base class:")
	aThree = Three()
	print("-> We are missing the call to B.__init__() !!!")


	print("\nDue to missing call of constructors, the variables of the base classe Two 'self.two' will not be accessible because they have never been even created:")

	print("See the value of self.Three: '%s'" % aThree.three)
	try:
		print("See the value of self.Two: " ),
		print("'%s'"  % aThree.two)
		
	except AttributeError as e:
		print("See, we get an exception:\n%s" % e)

	print("See the value of self.One: '%s'" % aThree.one)
		
    
    
if __name__ == "__main__":
	main()
    
#EOF    
